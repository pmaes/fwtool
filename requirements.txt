matplotlib==1.4.3
nose==1.3.7
numpy==1.10.0
pandas==0.16.2
pyparsing==2.0.3
python-dateutil==2.4.2
pytz==2015.6
six==1.9.0
