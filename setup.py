from setuptools import setup

setup(
    name="cellsine",
    version="0.0.1",
    author="Pieter Maes",
    description="CellSine data analysis",
    license='MIT',
    packages=['cellsine'],
    install_requires=[
        'pandas>=0.16',
        'matplotlib>=1.4.3'
    ]
)
