from collections import defaultdict
import numpy as np

# Convenience function to avoid miscounting of zeroes in literal alternative.
def kHz(x):
    return x * 1000

def distinct_colors(hls_seed):
    # Conjugate of the golden ratio
    spacing = 0.618033988749895
    h, l, s = hls_seed
    while True:
        yield (h, l, s)
        h = (h + spacing) % 1


def distinct_color_palette(hls_seed, size, l_tweak=0.05):
    h, l, s = hls_seed

    palette = []
    for idx, i in enumerate(np.linspace(0, 1, size, endpoint=False)):
        new_l = l+l_tweak if idx % 2 == 0 else l-l_tweak
        if new_l > 1.0 or new_l < 0.0:
            new_l = l
        new_h = (h+i) % 1
        palette.append((new_h, new_l, s))
    return palette


def sequential_color_palette(hls_seed, size):
    h, l, s = hls_seed
    step = (0.8 - l)/size

    colors = []
    for i in range(size):
        colors.append((h, l+step*i, s))
    return colors


def unique_names(names):
    seen = defaultdict(int)
    uniques = []
    for name in names:
        if name in seen:
            i = seen[name]
            unique = "{}{}".format(name, i+1)
        else:
            unique = name
        seen[name] += 1
        uniques.append(unique)
    return uniques
