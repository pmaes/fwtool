import os
import tempfile

from .utils import unique_names

import xlsxwriter
from IPython.display import FileLinks

def excel(*analyses):
    dir = tempfile.mkdtemp(dir='.')

    names = unique_names([analysis.abbrev_name for analysis in analyses])

    # TODO: Account for multiple experiments, stages
    prefix_analysis = analyses[0]
    prefix = prefix_analysis.dataset.specification.experiment_id + "_" +\
             prefix_analysis.dataset.specification.stages[0].activity

    fn = "_".join([prefix]+names)
    fn = "".join([x if x.isalnum() else "_" for x in fn])
    path = os.path.join(dir, '{}.xlsx'.format(fn))

    with xlsxwriter.Workbook(path, {'nan_inf_to_errors': True}) as workbook:
        bold = workbook.add_format({'bold': 1})

        for name, analysis in zip(names, analyses):
            worksheet = workbook.add_worksheet(name)

            worksheet.write_string(0, 0, str('measurement'), bold)

            for column, items in enumerate(analysis.data.items()):
                column += 1
                well, data = items
                worksheet.write_string(0, column, str(well), bold)

                for idx, value in enumerate(data):
                    row = idx + 1
                    worksheet.write_number(row, column, value)
                    worksheet.write_number(row, 0, idx)

    return FileLinks(dir)
