import colorsys
import cmath
from collections import OrderedDict, defaultdict, Counter
from functools import wraps

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

from .datastore import frequency_selector, parse_wells
from .utils import kHz, distinct_color_palette, sequential_color_palette


# TODO: get_name instead of static on class
# TODO: grouping of plots by stage, cell
# TODO: transforming n of measurement to time
# TODO: clamping of data to range (no peaks to 12000
# TODO: analyses should be composable
class DatasetAnalysis:
    base_name = ''
    base_label = 'Y'
    requires_baseline = False

    # Wrapping class initialization in case we want to do extra work here later.
    @classmethod
    def make_constructor(cls, **kwargs):
        def construct(*args, **inner_kwargs):
            inner_kwargs.update(kwargs)
            return cls(*args, **inner_kwargs)

        return construct

    def __init__(self, dataset, frequencies=None, normalized=False, diffed=False):
        if normalized and diffed:
            raise Exception('{} analysis cannot be both normalized and diffed'.format(self.name))
        self.normalized = normalized
        self.diffed = diffed

        self.dataset = dataset

        if not frequencies:
            frequencies = self.default_frequencies()
        self.selector = frequency_selector(frequencies)

        self.data = self._run()
        self.name = self._name()

    def _name(self):
        tmpl = "{}"
        if self.normalized:
            tmpl = "normalized {}"
        elif self.diffed:
            tmpl = "diff {}"

        return tmpl.format(self.base_name)

    @property
    def abbrev_name(self):
        return ''.join([s[0] for s in self.name.split(' ')])

    def default_frequencies(self):
        return None

    def _process_stage(self, activity_data, baseline_data, stage):
        if self.requires_baseline and baseline_data is None:
            raise Exception('{} analysis requires stage "{}" to have a baseline.'.format(self.name, stage.activity))

        return self.process_stage(self.selector(activity_data),
                                  self.selector(baseline_data),
                                  stage)

    def process_stage(self, activity, baseline, stage):
        return activity

    # Process the global data after it has been concatenated from process_stage output.
    def process_global(self, data):
        if self.normalized:
            return data / data.iloc[0]
        elif self.diffed:
            return data - data.iloc[0]
        else:
            return data

    # Returns well => global data array dict.
    def _run(self):
        stage_results = defaultdict(OrderedDict)

        for well, stage, activity, baseline in self.dataset.walk():
            stage_results[well][stage] = self._process_stage(activity, baseline, stage)

        return OrderedDict((well, self.process_global(pd.concat(stage_dict.values())))
                           for well, stage_dict in sorted(stage_results.items()))

    # Returns a function that, given a well, returns a (group_id, well_id) tuple, and the total number of groups.
    def _parse_groups(self, groups):
        group_dict = {}
        group_count = Counter()

        def update_groups(well, group_id, well_id=0):
            group_dict[well] = (group_id, well_id)
            group_count[group_id] += 1

        global_group_id = None
        # Add wells from defined groups.
        if groups:
            for group_id, well_specifiers in enumerate(groups):
                group_wells = parse_wells(well_specifiers)
                for well_id, well in enumerate(group_wells):
                    update_groups(well, group_id, well_id)
                global_group_id = group_id

        # Add the ungrouped wells to a singleton group
        for well in self.dataset.wells:
            if well not in group_dict:
                group_id = global_group_id + 1 if global_group_id is not None else 0
                update_groups(well, group_id)
                global_group_id = group_id

        def well_ids(well):
            _well_ids = group_dict[well]
            group_id, well_id = _well_ids
            n_wells = group_count[group_id]
            return _well_ids, n_wells

        return well_ids, global_group_id+1

    def _gen_well_palette(self, well_ids, n_groups):
        group_palette = distinct_color_palette((0.6016666666666667, 0.49411764705882355, 0.3968253968253968), n_groups)

        def well_palette(well):
            (group_id, well_id), n_wells = well_ids(well)
            group_color = group_palette[group_id % n_groups]
            intra_group_palette = sequential_color_palette(group_color, n_wells)
            well_color = intra_group_palette[well_id % n_wells]
            return colorsys.hls_to_rgb(*well_color)

        return well_palette

    def _plot_data(self):
        return self.data.items()

    def _plot_y_label(self):
        return self.base_label

    # TODO: Return figure so that multiple calls do not plot on the same figure
    # TODO: Facets (per freq)
    def plot(self, groups=None, label_size=14):
        well_palette = self._gen_well_palette(*self._parse_groups(groups))

        fig, ax = plt.subplots(1, 1)
        legend_labels = []
        for well, data in self._plot_data():
            # TODO: Stage/dataset identifier
            legend_labels.append("{}".format(well))
            ax.plot(data, color=well_palette(well))

        ax.set_ylabel(self._plot_y_label(), fontsize=label_size)
        ax.set_xlabel('measurements', fontsize=label_size)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])


        ax.legend(legend_labels, loc='upper center', bbox_to_anchor=(0.5, -0.2), ncol=4)


# Convenience function to follow the declarative function pattern for the end user.
def plot(analysis):
    analysis.plot()


def process_impedance(cartesian):
    def transform(df):
        if cartesian:
            return df.apply(lambda z: z.real), df.apply(lambda z: z.imag)
        else:
            return df.apply(lambda z: abs(z)), df.apply(lambda z: np.vectorize(cmath.phase)(z), axis=1)

    def decorator(func):
        @wraps(func)
        def wrapper(self, activity, baseline, stage):
            return func(self, transform(activity), transform(baseline), stage)
        return wrapper

    return decorator


class ImpedanceMagnitudeAnalysis(DatasetAnalysis):
    base_name = 'impedance magnitude'
    base_label = r'${}\vert Z \vert$'

    def _plot_y_label(self):
        prefix = ''
        if self.diffed:
            prefix = '\Delta'
        if self.normalized:
            prefix = 'normalized '

        return self.base_label.format(prefix)

    @process_impedance(cartesian=False)
    def process_stage(self, activity, baseline, stage):
        activity_magnitude, _ = activity
        return activity_magnitude


imp_magnitude = ImpedanceMagnitudeAnalysis.make_constructor()
diff_imp_magnitude = ImpedanceMagnitudeAnalysis.make_constructor(diffed=True)

# Legacy
impedance = imp_magnitude
diff_impedance = diff_imp_magnitude

# TODO: Full spectrum
class CellIndexAnalysis(DatasetAnalysis):
    base_name = 'cell index'
    base_label = '{} Cell Index'
    requires_baseline = True

    def _plot_y_label(self):
        prefix = ''
        if self.diffed:
            prefix = 'diff'
        if self.normalized:
            prefix = 'normalized'

        return self.base_label.format(prefix)

    def default_frequencies(self):
        return [kHz(10), kHz(25), kHz(50)]

    @process_impedance(cartesian=True)
    def process_stage(self, activity, baseline, stage):
        activity_resistance, _ = activity
        baseline_resistance, _ = baseline
        applied_baseline = baseline_resistance.mean()
        ci = ((activity_resistance / applied_baseline) - 1).max(1)
        return ci


cell_index = CellIndexAnalysis.make_constructor()
norm_cell_index = CellIndexAnalysis.make_constructor(normalized=True)
diff_cell_index = CellIndexAnalysis.make_constructor(diffed=True)
