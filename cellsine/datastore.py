import os
import fnmatch
import warnings
import re
import string
import itertools
from operator import itemgetter, attrgetter
from collections import OrderedDict, namedtuple, defaultdict
from functools import total_ordering

import pandas as pd


def closest_frequency(df, query):
    errs = ((abs(query - v), v) for v in df.columns.values.tolist())
    closest_err, closest_value = min(errs, key=itemgetter(0))
    return closest_value


def frequency_selector(queries=None):
    return lambda df: df[[closest_frequency(df, q) for q in queries]] if queries else df


# Experimental data is gathered for specific wells, fully specified by an alphabetic row and a numeric column.
@total_ordering
class Well(namedtuple('WellBase', ['row', 'column'])):
    # Currently, we assume all experiments are performed on 96-well plates.
    rows = string.ascii_lowercase[:8]
    columns = [str(c) for c in range(1, 13)]

    @classmethod
    def is_valid(cls, row, column):
        return row in cls.rows and column in cls.columns

    def __new__(cls, row, column):
        self = super().__new__(cls, row, column)
        self.identifier = "{}{}".format(row, column.zfill(2))

        # Check for validity of the coordinates when constructing the well.
        if not cls.is_valid(row, column):
            raise Exception("Well {} is not valid".format(self.identifier))

        return self

    @property
    def coords(self):
        return self.rows.index(self.row), self.columns.index(self.column)

    def __str__(self):
        return self.identifier

    def __lt__(self, other):
        return self.coords < other.coords


# Wells can be parsed from specifiers.
# Besides regular row and column specifiers, such as a4 or a04 (which are equivalent), a specifier can signify the
# exclusion of a well (-a04).
# Rows or columns can be replaced with a wildcard asterisk.
# TODO: Ranged identifiers such as a01:04
class WellSpecifier(namedtuple('WellSpecifierBase', ['row', 'column', 'exclude'])):
    @classmethod
    def from_string(cls, string):
        def _raise():
            raise Exception('Could not parse the well specifier "{}"'.format(string))

        m = re.match(r"(?P<subtract>-?)(?P<coordinates>((?P<row>[a-z]|\*)(?P<column>[0-9]+|\*))|\*)", string)
        if m:
            subtract, coordinates, row, column = m.group("subtract", "coordinates", "row", "column")
        else:
            _raise()

        # A minus sign in front signifies exclusion.
        exclude = subtract == "-"

        # A single wildcard is applied to both rows and columns.
        if coordinates == "*":
            row, column = "*", "*"

        # Remove the optional 0 prefix of the column.
        if column != "*":
            column = column.lstrip('0')

        return cls(row, column, exclude)

    # Parsing a well specifier consists of generating all the wells that it describes.
    # Return tuples of wells with their strictness.
    def parse(self):
        # Apply the wildcards by simply including all possible coordinates in their respective sets.
        # Instead of actual sets, we use lists to retain expected order.
        row_set, row_strict = (Well.rows, False) if self.row == '*' else ([self.row], True)
        column_set, column_strict = (Well.columns, False) if self.column == '*' else ([self.column], True)

        # The wells should be applied strictly only if both the row and column specifiers were not wildcards.
        strict = row_strict and column_strict

        # The resulting set of wells is simply the product of all possible rows and columns.
        wells = (Well(r, c) for r, c in itertools.product(row_set, column_set))

        return [(well, strict) for well in wells]


# Given an iterable of specifiers, return a map of the wells thereby specified to their strictness.
def parse_well_specifiers(specifiers):
    # Split the generated wells by whether the associated specifier is to include or exclude.
    included, excluded = [], []
    for specifier in specifiers:
        (excluded if specifier.exclude else included).extend(specifier.parse())

    excluded = set(well for well, strict in excluded)

    # Well strictness wins over non-strictness.
    parsed = defaultdict(lambda: False)
    for well, strict in included:
        if well not in excluded:
            parsed[well] |= strict
    return parsed


def parse_wells(strings):
    return parse_well_specifiers(WellSpecifier.from_string(string) for string in strings)


# Convenience method to be used in interactive sessions.
def wells(*strings):
    return parse_wells(strings)


# A stage associates an activity measurement with metadata such as the applicable baseline.
# A list of stages combined with the wells fully describes the data in a dataset.
class Stage(namedtuple('StageBase', ['activity', 'baseline'])):
    # A stage's identifier set contains all the identifiers through which the stage is defined.
    @property
    def identifier_set(self):
        stages = {self.activity}
        if self.baseline:
            stages.add(self.baseline)
        return stages


# The stage construction function returns a list of stages, applying the kwarg metadata to every identifier.
def stage(*identifiers, baseline=None):
    return [Stage(activity=identifier, baseline=baseline) for identifier in identifiers]


# Multiple stage definitions are consolidated by flattening the resulting lists.
def consolidate_stages(stages):
    return list(itertools.chain.from_iterable(stages))


# A Dataset is specified by a combination of wells and stages from an experiment.
# The wells should be keys in a dict mapping them to their strictness.
class DatasetSpecification:
    def __init__(self, well_map, stages, experiment_id):
        self.well_map = well_map
        self.wells = list(well_map.keys())
        self.stages = consolidate_stages(stages)
        self.experiment_id = experiment_id
        # The set of identifiers in all the stages of the specification.
        self.stage_identifier_set = set.union(*[stage.identifier_set for stage in self.stages])

    def is_strict_well(self, well):
        return self.well_map[well]


# A Dataset is constructed from a specification and a nested dict of well => stage_id => dataframe
class Dataset:
    # The data for a specification is constructed by concatenating the dataframes in the order of the specified stages.
    # The offsets of the stages are kept in a separate list of (offset, baseline) tuples.
    @staticmethod
    def _specification_data(specification, data_dict):
        # Activity data is concatenated into a single array.
        # Baseline data is stored in a map from a stage id.
        def stage_data(stage_dict):
            # Seed with first stages' activity.
            first_stage = specification.stages[0]
            activity_data = stage_dict[first_stage.activity]
            stage_offsets = OrderedDict([(first_stage, 0)])

            for stage in specification.stages[1:]:
                stage_offsets[stage] = len(activity_data)

                # Offset indices of new data to follow global activity.
                stage_activity = stage_dict[stage.activity]
                stage_activity.index = stage_activity.index + activity_data.index[-1]

                activity_data = activity_data.append(stage_activity)

            baseline_data = {}
            for stage in specification.stages:
                baseline_data[stage.baseline] = stage_dict[stage.baseline] if stage.baseline else None

            return (activity_data, baseline_data), stage_offsets

        # well => (activity, baseline)
        data = {}
        offsets = {}
        for well, stage_dict in data_dict.items():
            data[well], offsets[well] = stage_data(stage_dict)

        return data, offsets

    def __init__(self, specification, data_dict):
        self.specification = specification
        self.data, self.stage_offsets = self._specification_data(specification, data_dict)
        self.wells = list(sorted(self.data.keys()))

    # Return start:end indices of stage in global data array.
    def _stage_bounds(self, well, stage):
        offsets = self.stage_offsets[well]
        start = offsets[stage]
        stage_ids = list(offsets.keys())
        idx = stage_ids.index(stage)
        # Not the last stage, go until next stage.
        if idx < len(stage_ids) - 1:
            end = offsets[stage_ids[idx + 1]]
        # Last stage, simply go until the end of the global array.
        else:
            end = len(self.data[well][0])
        return start, end

    # Yields (well, stage, activity, baseline) tuples for every well & stage in the dataset.
    # Most data access should happen through this method, so that addition of eventual metadata can be handled easily.
    def walk(self):
        for well, data in self.data.items():
            activity, baseline = data

            for stage in self.stage_offsets[well].keys():
                slice_start, slice_end = self._stage_bounds(well, stage)
                stage_activity = activity.iloc[slice_start:slice_end]

                stage_baseline = baseline[stage.baseline]

                yield well, stage, stage_activity, stage_baseline


def read_impedance_file(fn):
    with open(fn, 'r') as f:
        return [map(float, l.split()) for l in f.readlines()]


# t is an optional integer tagging the point in time, which will be used as the index in the DataFrame.
def process_raw_frequency_data(freq_data, t=None):
    df = pd.DataFrame(freq_data, columns=['frequency', 'real', 'imag'])
    df.set_index(df['frequency'], inplace=True)
    df['impedance'] = df[['real', 'imag']].apply(lambda comps: complex(*comps), axis=1)
    del df['imag']
    del df['real']
    del df['frequency']
    df = df.transpose()

    if t:
        t_index = pd.Series(list(itertools.repeat(t, len(df))))
        df.set_index(t_index, inplace=True)

    return df


def load_frequency_data(fn, compressed=False, t=None):
    return process_raw_frequency_data(read_impedance_file(fn), t)


ParsedFilename = namedtuple('ParsedFilename', ['path', 'cell', 'order'])


def parse_filename(fn, root=None):
    m = re.match(r".*cell([a-z][0-9]{2})_([0-9]+).*", fn)
    if m:
        cell, order_s = m.group(1, 2)
        order = int(order_s)

        path = os.path.join(root, fn) if root else fn
        return ParsedFilename(path, cell, order)
    else:
        raise Exception("Filename '{}'' could not be parsed".format(fn))


# Takes a map of id's to patterns and returns a filter that given filenames returns a map of id's to the fnmatch set.
def _patterns_fnfilter(patterns, deduplicate=False):
    # Implement filter over iterable of filenames using fnmatch.filter instead of fnmatch.fnmatch because the former is
    # implemented more efficiently than a simple comprehension.
    def filter(fns):
        matches = defaultdict(set)
        # Keep track of strings already matched to deduplicate.
        matched = set()
        for id, pattern in sorted(patterns.items(), key=lambda t: len(t[0]), reverse=True):
            new_matches = set(fnmatch.filter(fns, pattern))
            if deduplicate:
                new_matches -= matched
                matched |= new_matches
            matches[id] |= new_matches
        return matches

    return filter


# Returns a filter that matches the intersection of the passed filters, the new keys being a tuple of the individual
# filter's.
def _compose_fnfilter(*filters):
    def filter(fns):
        matches = None
        for f in filters:
            if not matches:
                matches = f(fns)
            else:
                # Bit of a hacky use of sum to flatten the key tuple
                matches = {sum(((old_key,), (new_key,)), ()): old_matches & new_matches
                           for old_key, old_matches in matches.items()
                           for new_key, new_matches in f(fns).items()}
        return matches

    return filter


class DataStore:
    _shared_instance = None
    @classmethod
    def shared_store(cls, root):
        # TODO: If None root, get it from the env variables
        if cls._shared_instance and cls._shared_instance.root == root:
            shared_instance = cls._shared_instance
        else:
            shared_instance = cls(root)
            cls._shared_instance = shared_instance
        return shared_instance

    def __init__(self, root):
        self.root = root

    @staticmethod
    def _parse_identifier(identifier):
        m = re.match(r"(\w+)\/(\w+)\/(\w+)", identifier)
        if m:
            name_identifier, group, experiment_name = m.group(1, 2, 3)
            return name_identifier, group, experiment_name
        else:
            raise Exception("Identifier {} does not follow expected format.".format(identifier))

    def _path_for_identifier(self, identifier):
        path = os.path.join(self.root, *self._parse_identifier(identifier))
        if not os.path.isdir(path):
            raise Exception("No directory found corresponding to identifier {}".format(identifier))
        return path

    # Return all the files for a specific experiment.
    def _files_for_experiment(self, experiment_id):
        # Only look in the top level of the path
        root, dirnames, filenames = next(os.walk(self._path_for_identifier(experiment_id)))
        return filenames

    # Returns a dict of wells => stage => files.
    def select_files(self, specification):
        # The specified files are at the intersection of a filter that selects the wells and one that selects the
        # stages.
        file_filter = _compose_fnfilter(_patterns_fnfilter({well: '*cell{}*.z'.format(well.identifier)
                                                            for well in specification.wells}),
                                        _patterns_fnfilter({id: '*_{}_cell*.z'.format(id)
                                                            for id in specification.stage_identifier_set},
                                                           deduplicate=True))

        # TODO: Raise exception when path does not exist
        matches = file_filter(self._files_for_experiment(specification.experiment_id))

        # A nested dict of well => stage => files
        files = defaultdict(dict)
        # A map of the stage id to the wells matched.
        stage_wells = defaultdict(set)

        for (well, stage_id), match_fns in matches.items():
            if specification.is_strict_well(well) and not match_fns:
                warnings.warn("No files found for identifier {} in well {}.".format(stage_id, well.identifier))

            if match_fns:
                # Sort parsed filenames by order
                files[well][stage_id] = sorted((parse_filename(fn,
                                                               root=self._path_for_identifier(
                                                                   specification.experiment_id))
                                                for fn in match_fns),
                                               key=attrgetter('order'))
                stage_wells[stage_id].add(well)

        # We only want to keep the wells for which we have all the stage ids.
        # If this means we have to drop a strict well, warn the user.
        common_wells = set.intersection(*stage_wells.values())
        for well in set(files.keys()):
            if well not in common_wells:
                # Remove the wells for which not all stages were found from the files dict.
                files.pop(well, None)

                if specification.is_strict_well(well):
                    warnings.warn("Not all specified files were found for well {}.".format(well.identifier))

        return files

    # TODO: Check order for missing data
    # TODO: Caching
    def load_data_set(self, specification):
        data_dict = self.select_files(specification)

        for well, stage_ids in data_dict.items():
            for stage_id, parsed_fns in stage_ids.items():
                stage_ids[stage_id] = pd.concat(load_frequency_data(parsed_fn.path, t=parsed_fn.order)
                                                for parsed_fn in parsed_fns)

        return Dataset(specification, data_dict)


def dataset(wells, stages, experiment, data_store=None):
    spec = DatasetSpecification(parse_wells(wells), stages, experiment)
    if not data_store:
        data_store = DataStore.shared_store()
    return data_store.load_data_set(spec)
